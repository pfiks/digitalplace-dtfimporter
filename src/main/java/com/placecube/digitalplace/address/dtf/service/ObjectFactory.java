package com.placecube.digitalplace.address.dtf.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.dbcp2.BasicDataSource;

import com.placecube.digitalplace.address.dtf.constant.DTFConstants;
import com.placecube.digitalplace.address.dtf.model.Blpu;
import com.placecube.digitalplace.address.dtf.model.DTFDatabaseRecord;
import com.placecube.digitalplace.address.dtf.model.DTFRecord;
import com.placecube.digitalplace.address.dtf.model.Lpi;
import com.placecube.digitalplace.address.dtf.model.StreetDescriptor;
import com.placecube.digitalplace.address.dtf.model.StreetRecord;

public final class ObjectFactory {

	private ObjectFactory() {

	}
	
	public static LocalDateTime getTimeNow() {
		return LocalDateTime.now();
	}

	public static List<String> newArrayList() {
		return new ArrayList<>();
	}
	
	public static  Set<String> newHashSet() {
		return new HashSet<>(); 
	}

	public static DTFParseService createFileParseService(DatabaseService databaseService) {
		return DTFParseService.fromDatabaseService(databaseService);
	}

	public static DTFRecord createDTFRecord(CSVRecord csvRecord, int versionId) {
		return new DTFRecord(csvRecord, versionId);
	}
	
	public static DTFDatabaseRecord createDTFDatabaseRecord(DTFRecord dtfRecord) {
		int recordIdentifier = dtfRecord.getRecordIdentifier();

		if (recordIdentifier == DTFConstants.INDENTIFIER_STREET_RECORD) {

			return createStreetRecord(dtfRecord);

		} else if (recordIdentifier == DTFConstants.INDENTIFIER_STREET_DESCRIPTOR) {

			return createStreetDescriptor(dtfRecord);

		} else if (recordIdentifier == DTFConstants.INDENTIFIER_BLPU) {

			return createBlpu(dtfRecord);

		} else if (recordIdentifier == DTFConstants.INDENTIFIER_LPI) {

			return createLpi(dtfRecord);
		}

		return null;
	}

	public static StreetRecord createStreetRecord(DTFRecord dtfRecord) {
		return new StreetRecord(dtfRecord);
	}

	public static StreetDescriptor createStreetDescriptor(DTFRecord dtfRecord) {
		return new StreetDescriptor(dtfRecord);
	}

	public static Blpu createBlpu(DTFRecord dtfRecord) {
		return new Blpu(dtfRecord);
	}

	public static Lpi createLpi(DTFRecord dtfRecord) {
		return new Lpi(dtfRecord);
	}

	public static BasicDataSource createDataSource() {
		return new BasicDataSource();
	}

	public static Properties createProperties(String pathToProperties) throws Exception {
		
		try(InputStream fileInputStream = new FileInputStream(new File(pathToProperties))) {
			Properties properties = new Properties();
			properties.load(fileInputStream);
			return properties;
		}
	
	}
	
	public static DatabaseService createDatabaseService(Properties configProperties) {
		return DatabaseService.fromConfigProperties(configProperties);
	}
	
	public static ActionExecutorService createActionExecutorService(ArgumentsService argumentsService) throws Exception {
		return ActionExecutorService.fromArgumentsService(argumentsService);
	}

	public static ArgumentsService createArgumentsService(String args[]) {
		return ArgumentsService.fromArgs(args);
	}
	
	public static FileAndFolderService createFileAndFolderService(DatabaseService databaseService) {
		return new FileAndFolderService(databaseService);
	}
	
	public static FileInputStream createFileInputStream(File file) throws FileNotFoundException {
		return new FileInputStream(file);
	}
	
	public static InputStreamReader createInputStreamReader(FileInputStream fileInputStream) throws FileNotFoundException {
		return new InputStreamReader(fileInputStream);
	}
	
	public static Iterable<CSVRecord> createCsvRecordIterator(Reader reader) throws IOException {
		return CSVFormat.EXCEL.withAllowMissingColumnNames().parse(reader);
	}
	
}
