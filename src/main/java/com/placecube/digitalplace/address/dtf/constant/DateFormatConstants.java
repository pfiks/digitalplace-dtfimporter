package com.placecube.digitalplace.address.dtf.constant;

public final class DateFormatConstants {
	
	public static final String FILENAME_DATE_FORMAT = "yyyyMMdd";
	
	public static final String DTF_RECORD_DATE_FORMAT = "yyyy-MM-dd";
	
	private DateFormatConstants() {
		
	}

}
