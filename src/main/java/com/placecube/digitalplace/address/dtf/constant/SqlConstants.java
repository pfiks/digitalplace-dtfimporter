package com.placecube.digitalplace.address.dtf.constant;

public final class SqlConstants {
	
	public static final String ADD_VERSION = "insert into Version values(null, '%s', null, '%s', null);";
	
	public static final String CLEAR_ALL = "call spTruncateEverything();";
	
	public static final String DELETE_BY_UPRN = "delete from %s where UPRN = '%s'";
	
	public static final String DELETE_BY_USRN = "delete from %s where USRN = '%s'";
	
	public static final String GET_MATCHING_FILE = "SELECT * FROM Version where SourceFileName like '%%%s%%';";
	
	public static final String INSERT_BLPU = "spInsertIntoBLPU";
	
	public static final String INSERT_DTF_LINE = "spInsertIntoDtfLine";
	
	public static final String INSERT_LPI = "spInsertIntoLPI";
	
	public static final String INSERT_STREET_DESCRIPTOR = "spInsertIntoStreetDescriptor";
	
	public static final String INSERT_STREET_RECORD = "spInsertIntoStreetRecord";
	
	public static final String TABLE_BLPU = "BLPU";
	
	public static final String TABLE_LINE = "DtfLine";
	
	public static final String TABLE_LPI = "LPI";
	
	public static final String TABLE_STREET_DESCRIPTOR = "StreetDescriptor";
	
	public static final String TABLE_STREET_RECORD = "StreetRecord";
	
	public static final String UPDATE_VERSION_MESSAGE = "update Version set messages = '%s', EndDateTime= '%s' where versionid = %d";
	
	private SqlConstants() {
		
	}

}
