package com.placecube.digitalplace.address.dtf.service;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.placecube.digitalplace.address.dtf.constant.SqlConstants;
import com.placecube.digitalplace.address.dtf.model.DTFChangeType;
import com.placecube.digitalplace.address.dtf.model.DTFDatabaseRecord;
import com.placecube.digitalplace.address.dtf.model.DTFRecord;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest({ ObjectFactory.class, CSVRecord.class, LocalDateTime.class })
public class DTFParseServiceTest {

	private static final DTFChangeType CHANGE_TYPE = DTFChangeType.INSERT;

	private DTFParseService dtfParseService;

	private Set<String> processedFiles = new HashSet<>();

	@Mock
	private List<String> mockMessages;

	@Mock
	private DTFDatabaseRecord mockDTFDatabaseRecord;

	@Mock
	private DTFRecord mockDTFRecord;

	@Mock
	private CSVRecord mockCsvRecord;

	@Mock
	private DatabaseService mockDatabaseService;

	@Mock
	private File mockFile;

	@Mock
	private FileInputStream mockFileInputStream;

	@Mock
	private InputStreamReader mockFileInputStreamReader;

	@Before
	public void setUp() throws Exception {
		initMocks(this);
		PowerMockito.mockStatic(ObjectFactory.class, LocalDateTime.class);

		when(mockFile.getAbsolutePath()).thenReturn("filename");

		when(ObjectFactory.createFileInputStream(mockFile)).thenReturn(mockFileInputStream);
		when(ObjectFactory.createInputStreamReader(mockFileInputStream)).thenReturn(mockFileInputStreamReader);
		when(ObjectFactory.createDTFDatabaseRecord(mockDTFRecord)).thenReturn(mockDTFDatabaseRecord);
		when(ObjectFactory.createDTFRecord(mockCsvRecord, 0)).thenReturn(mockDTFRecord);
		when(ObjectFactory.createCsvRecordIterator(mockFileInputStreamReader)).thenReturn(getDummyCsvRows());
		when(ObjectFactory.newHashSet()).thenReturn(processedFiles);
		when(ObjectFactory.newArrayList()).thenReturn(mockMessages);
		when(ObjectFactory.getTimeNow()).thenReturn(LocalDate.ofEpochDay(1).atStartOfDay());

		// when(LocalDateTime.now()).thenReturn(LocalDate.ofEpochDay(1).atStartOfDay());

		dtfParseService = DTFParseService.fromDatabaseService(mockDatabaseService);
	}

	@Test
	public void updateDatabaseFromFile_whenIsPermittedChangeTypeAndIdentifier_thenDatabaseProcessRecordInvoked()
			throws Exception {

		when(mockDTFRecord.isPermittedChangeTypeAndIdentifier(CHANGE_TYPE)).thenReturn(true);

		dtfParseService.updateDatabaseFromFile(mockFile, CHANGE_TYPE);

		verify(mockDatabaseService, times(1)).processDtfDatabaseRecord(mockDTFDatabaseRecord, CHANGE_TYPE);

	}

	@Test
	public void updateDatabaseFromFile_whenNotPermittedChangeTypeAndIdentifier_thenDatabaseProcessRecordNotInvoked()
			throws Exception {

		when(mockDTFRecord.isPermittedChangeTypeAndIdentifier(CHANGE_TYPE)).thenReturn(false);

		dtfParseService.updateDatabaseFromFile(mockFile, CHANGE_TYPE);

		verify(mockDatabaseService, never()).processDtfDatabaseRecord(mockDTFDatabaseRecord, CHANGE_TYPE);

	}

	@Test
	public void updateDatabaseFromFile_whenFileIsProcessed_thenVersionIdIncrementedInDb() throws Exception {
		String sql = String.format(SqlConstants.ADD_VERSION, ObjectFactory.getTimeNow(), mockFile.getAbsolutePath());

		dtfParseService.updateDatabaseFromFile(mockFile, CHANGE_TYPE);

		verify(mockDatabaseService, times(1)).runSqlWithResult(sql);

	}

	@Test
	public void updateDatabaseFromFile_whenFileHasBeenPreviouslyProcess_thenVersionNotIncrementedInDb()
			throws Exception {
		processedFiles.add(mockFile.getAbsolutePath());
		String sql = String.format(SqlConstants.ADD_VERSION, ObjectFactory.getTimeNow(), mockFile.getAbsolutePath());

		dtfParseService.updateDatabaseFromFile(mockFile, CHANGE_TYPE);

		verify(mockDatabaseService, never()).runSqlWithResult(sql);

	}

	@Test
	public void updateDatabaseFromFile_whenFileIsFinished_thenMessagesForVersionAreUpdatedInDb() throws Exception {

		String sql = String.format(SqlConstants.UPDATE_VERSION_MESSAGE, StringUtils.join(mockMessages, ";\r\n"),
				ObjectFactory.getTimeNow(), 0);

		dtfParseService.updateDatabaseFromFile(mockFile, CHANGE_TYPE);

		verify(mockDatabaseService, times(1)).runSql(sql);

	}

	private List<CSVRecord> getDummyCsvRows() {
		List<CSVRecord> dummyRecords = new ArrayList<>();
		dummyRecords.add(mockCsvRecord);

		return dummyRecords;
	}

}
