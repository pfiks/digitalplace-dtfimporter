package com.placecube.digitalplace.address.dtf.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.placecube.digitalplace.address.dtf.constant.SqlConstants;
/*
 * Land and Property Identifier - DTF Record Identifier 24
 */
public class Lpi implements DTFDatabaseRecord {
	
	private DTFRecord dtfRecord;
	
	private Map<LpiField, Object> fields = new HashMap<>();
	
	public Lpi(DTFRecord dtfRecord) {
		this.dtfRecord = dtfRecord;
		for (LpiField lpiField : LpiField.values()) {
			int columnIndex = lpiField.getColumnNumber();
			fields.put(lpiField, dtfRecord.columnValueToObject(columnIndex, lpiField.getFieldClass()));
		}
		
	}

	@Override
	public List<Object> getDatabaseInsertParameters() {
		List<Object> params = new ArrayList<>();
		
		for (LpiField lpiField : LpiField.getFieldsInPositionOrder()) {
			params.add(fields.get(lpiField));
		}
		
		params.add(dtfRecord.getVersionId());

		return params;
	}

	@Override
	public String getDeleteSql() {
		return String.format(SqlConstants.DELETE_BY_UPRN, SqlConstants.TABLE_LPI, dtfRecord.getColumnValue(LpiField.UPRN.getColumnNumber()));
	}

	@Override
	public String getInsertSql() {
		return SqlConstants.INSERT_LPI;
	}

}
