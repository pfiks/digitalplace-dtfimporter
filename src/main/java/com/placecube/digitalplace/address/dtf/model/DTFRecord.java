package com.placecube.digitalplace.address.dtf.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import com.placecube.digitalplace.address.dtf.constant.DTFConstants;
import com.placecube.digitalplace.address.dtf.constant.DateFormatConstants;

public class DTFRecord {
	
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DateFormatConstants.DTF_RECORD_DATE_FORMAT);
	
	private CSVRecord dtfCsvRecord;

	private int versionId;
	
	public DTFRecord(CSVRecord dtfCsvRecord, int versionId) {
		this.dtfCsvRecord = dtfCsvRecord;
		setVersionId(versionId);
	}

	public DTFChangeType getDtfChangeType() {
		DTFField dtfField = DTFField.RECORD_CHANGE_TYPE;
		String changeType = (String)columnValueToObject(dtfField.getColumnNumber(), dtfField.getFieldClass());
		return DTFChangeType.getChangeTypeFromString(changeType);
	}

	public long getProOrder() {
		DTFField dtfField = DTFField.RECORD_PRO_ORDER;
		return (long)columnValueToObject(dtfField.getColumnNumber(), dtfField.getFieldClass());
	}
	
	public int getRecordIdentifier() {
		DTFField dtfField = DTFField.RECORD_IDENTIFIER;
		return (int)columnValueToObject(dtfField.getColumnNumber(), dtfField.getFieldClass());
	}
	
	public String getColumnValue(int columnIndex) {
		return dtfCsvRecord.get(columnIndex);
	}

	public int getVersionId() {
		return versionId;
	}
	
	public void setVersionId(int versionId) {
		this.versionId = versionId;
	}
	
	public Object columnValueToObject(int columnIndex, Class<?> fieldType) {
		String value = dtfCsvRecord.get(columnIndex);
		
		if (fieldType == LocalDate.class) {
			return getDate(value);
		} else if (fieldType == Long.class) {
			return getLong(value);
		} else if (fieldType == BigDecimal.class) {
			return getDecimal(value);
		} else if (fieldType == Integer.class) {
			return getInteger(value);
		}
		
		return value;
	}
	
	private LocalDate getDate(String value) {
		if (StringUtils.isNotEmpty(value)) {
			LocalDate ld = LocalDate.parse(value.trim(), DATE_FORMATTER);
			return ld.atStartOfDay(ZoneOffset.UTC).toLocalDate();
		}

		return null;
	}

	private BigDecimal getDecimal(String value) {
		if (StringUtils.isNotEmpty(value)) {
			return new BigDecimal(value.trim());	
		}

		return null;

	}
	
	private Long getLong(String value) {
		if (StringUtils.isNotEmpty(value)) {
			return Long.parseLong(value.trim());
		}
		
		return null;
	}
	
	private Integer getInteger(String value) {
		if (StringUtils.isNotEmpty(value)) {
			return Integer.parseInt(value.trim());
		}
		
		return null;
	}
	
	public boolean isPermittedChangeTypeAndIdentifier(DTFChangeType permittedChangeType) {
		
		return DTFConstants.ALLOWED_RECORD_INDENTIFIERS.contains(getRecordIdentifier())
				&& permittedChangeType == getDtfChangeType();
	}

}