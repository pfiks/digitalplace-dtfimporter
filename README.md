﻿## Summary

This project loads a file in DTF7.3 format into a database. 

DTF7.3 is the Data Transfer Format specification used by local councils to export their address data from their LLPG, in a format matching BS 7666:2006.

The data is loaded into a set of database tables - BLPUs, LPIs and Street Descriptors making it easy to list all the addresses and use sql to query them.

The code also supports DTF delta files, where records are updated and deleted as well as inserted.

## Setup

Prepare the database for importing data as follows:

* Create a blank SQL database
* Run "digitalplace-dtfimporter/src/main/resources/db/schema.sql" to create the schema

Once the database is setup, the importer can either be run as a JBang script (recommended) in a Unix environment or as a standard Java project with precompiled binaries built using Gradle. The JBang script approach needs no manual setup and will install all depedencies including Java.

## Running the importer as a JBang script
* Copy `src/main/resources/config.properties` somewhere and modify accordingly. The `dtf.source.file.or.folder` property can be used to either configure the path to a single file for import or a folder containing multiple files. If a folder is specified then the folder will be scanned for ".csv" files. The scan is NOT recursive.
* Import data into the database with `./jbang src/main/java/com/placecube/digitalplace/address/dtf/Program.java loadfile <CONFIG_PROPERTIES_PATH>` from the root directory of the repo, replacing `<CONFIG_PROPERTIES_PATH>` with your new config.properties path.
* Wipe out all database data with `./jbang src/main/java/com/placecube/digitalplace/address/dtf/Program.java clearall <CONFIG_PROPERTIES_PATH>` from the root directory of the repo, replacing `<CONFIG_PROPERTIES_PATH>` with your new config.properties path.
  
## Running the importer as a standard Java project
* Use `./gradlew clean build distZip` to build the project
* Once built, go to the directory containing the zip file e.g. "digitalplace-dtfimporter/build/distributions/digitalplace-dtfimporter.zip"
* Extract the zip file into a folder and enter the "digitalplace-dtfimporter" folder. From here you should run the application.
* There is a sample "config.properties" within the jar "digitalplace-dtfimporter.jar". Extract this and modify accordingly.
* The `dtf.source.file.or.folder` property can be used to either configure the path to a single file for import or a folder containing multiple files. If a folder is specified then the folder will be scanned for ".csv" files. The scan is NOT recursive.
* Import data into the database with `java -cp "lib/*:digitalplace-dtfimporter.jar" com.placecube.digitalplace.address.dtf.program.Program loadfile <CONFIG_PROPERTIES_PATH>` replacing `<CONFIG_PROPERTIES_PATH>` with your new config.properties path.
* Wipe out all database data with `java -cp "lib/*:digitalplace-dtfimporter.jar" com.placecube.digitalplace.address.dtf.program.Program clearall <CONFIG_PROPERTIES_PATH>` replacing `<CONFIG_PROPERTIES_PATH>` with your new config.properties path.


## License
Copyright (C) 2019-present Placecube Limited.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
