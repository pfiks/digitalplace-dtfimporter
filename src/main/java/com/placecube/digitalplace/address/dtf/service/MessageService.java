package com.placecube.digitalplace.address.dtf.service;

public final class MessageService {
	
	private MessageService() {
		
	}
	
	public static void printMessage(String message) {
		System.out.println(message);
	}

}
