package com.placecube.digitalplace.address.dtf.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.apache.commons.csv.CSVRecord;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.placecube.digitalplace.address.dtf.constant.DTFConstants;
import com.placecube.digitalplace.address.dtf.service.ObjectFactory;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest({ CSVRecord.class })
public class DTFRecordTest {
	
	private DTFRecord dtfRecord;
	
	private static final DTFChangeType CHANGE_TYPE = DTFChangeType.INSERT;
	
	private static final int RECORD_IDENTIFIER = DTFConstants.INDENTIFIER_BLPU;
	
	private static final long RECORD_PRO_ORDER = 1l;
	
	private static final int UNPERMITTED_IDENTIFIER = 99;
	
	private static final int VERSION = 1;
	
	@Mock
	private CSVRecord mockCsvRecord;
	
	@Before
	public void setUp() {
		
		initMocks(this);
		
		dtfRecord = ObjectFactory.createDTFRecord(mockCsvRecord, VERSION);
		
	}
	
	@Test
	public void getDtfChangeType_whenSetInCsvRecord_thenCorrectChangeTypeIsReturned() {
		mockCSVRecordAndInstatiateTestInstance();
		
		DTFChangeType changeType = dtfRecord.getDtfChangeType();
		
		assertEquals(CHANGE_TYPE, changeType);
	}
	
	@Test
	public void getProOrder_whenSetInCsvRecord_thenCorrectProOrderIsReturned() {
		mockCSVRecordAndInstatiateTestInstance();
		
		long proOrder = dtfRecord.getProOrder();
		
		assertEquals(RECORD_PRO_ORDER, proOrder);
	}
	
	@Test
	public void getRecordIdentifier_whenSetInCsvRecord_thenCorrectRecordIdentifierIsReturned() {
		mockCSVRecordAndInstatiateTestInstance();
		
		int recordIdentifier = dtfRecord.getRecordIdentifier();
		
		assertEquals(RECORD_IDENTIFIER, recordIdentifier);
	}
	
	@Test
	public void getVersionId_whenPassedToContructor_thenCorrectVersionIdIsReturned() {
		mockCSVRecordAndInstatiateTestInstance();
		
		int versionId = dtfRecord.getVersionId();
		
		assertEquals(VERSION, versionId);
		
	}
	
	@Test
	public void isPermittedChangeTypeAndIdentifier_whenBlpuIdAndPermittedChangeType_thenTrueIsReturned() {
		String recordChangeType = DTFChangeType.INSERT.getDtfRecordRepresentation();
		DTFChangeType permittedChangeType = DTFChangeType.INSERT;
		
		when(mockCsvRecord.get(DTFField.RECORD_CHANGE_TYPE.getColumnNumber())).thenReturn(recordChangeType);
		when(mockCsvRecord.get(DTFField.RECORD_IDENTIFIER.getColumnNumber())).thenReturn(String.valueOf(DTFConstants.INDENTIFIER_BLPU));
		
		assertTrue(dtfRecord.isPermittedChangeTypeAndIdentifier(permittedChangeType));
		
	}
	
	@Test
	public void isPermittedChangeTypeAndIdentifier_whenLpiIdAndPermittedChangeType_thenTrueIsReturned() {
		String recordChangeType = DTFChangeType.INSERT.getDtfRecordRepresentation();
		DTFChangeType permittedChangeType = DTFChangeType.INSERT;
		
		when(mockCsvRecord.get(DTFField.RECORD_CHANGE_TYPE.getColumnNumber())).thenReturn(recordChangeType);
		when(mockCsvRecord.get(DTFField.RECORD_IDENTIFIER.getColumnNumber())).thenReturn(String.valueOf(DTFConstants.INDENTIFIER_LPI));
		
		assertTrue(dtfRecord.isPermittedChangeTypeAndIdentifier(permittedChangeType));
		
	}
	
	@Test
	public void isPermittedChangeTypeAndIdentifier_whenStreetDescriptorIdAndPermittedChangeType_thenTrueIsReturned() {
		String recordChangeType = DTFChangeType.INSERT.getDtfRecordRepresentation();
		DTFChangeType permittedChangeType = DTFChangeType.INSERT;
		
		when(mockCsvRecord.get(DTFField.RECORD_CHANGE_TYPE.getColumnNumber())).thenReturn(recordChangeType);
		when(mockCsvRecord.get(DTFField.RECORD_IDENTIFIER.getColumnNumber())).thenReturn(String.valueOf(DTFConstants.INDENTIFIER_STREET_DESCRIPTOR));
		
		assertTrue(dtfRecord.isPermittedChangeTypeAndIdentifier(permittedChangeType));
		
	}
	
	@Test
	public void isPermittedChangeTypeAndIdentifier_whenStreetRecordIdAndPermittedChangeType_thenTrueIsReturned() {
		String recordChangeType = DTFChangeType.INSERT.getDtfRecordRepresentation();
		DTFChangeType permittedChangeType = DTFChangeType.INSERT;
		
		when(mockCsvRecord.get(DTFField.RECORD_CHANGE_TYPE.getColumnNumber())).thenReturn(recordChangeType);
		when(mockCsvRecord.get(DTFField.RECORD_IDENTIFIER.getColumnNumber())).thenReturn(String.valueOf(DTFConstants.INDENTIFIER_STREET_RECORD));
		
		assertTrue(dtfRecord.isPermittedChangeTypeAndIdentifier(permittedChangeType));
		
	}
	
	@Test
	public void isPermittedChangeTypeAndIdentifier_whenUnpermittedRecordIdAndPermittedChangeType_thenFalseIsReturned() {
		String recordChangeType = DTFChangeType.INSERT.getDtfRecordRepresentation();
		DTFChangeType permittedChangeType = DTFChangeType.INSERT;
		
		when(mockCsvRecord.get(DTFField.RECORD_CHANGE_TYPE.getColumnNumber())).thenReturn(recordChangeType);
		when(mockCsvRecord.get(DTFField.RECORD_IDENTIFIER.getColumnNumber())).thenReturn(String.valueOf(UNPERMITTED_IDENTIFIER));
		
		assertFalse(dtfRecord.isPermittedChangeTypeAndIdentifier(permittedChangeType));
		
	}
	
	@Test
	public void isPermittedChangeTypeAndIdentifier_whenUnpermittedRecordIdAndUnPermittedChangeType_thenFalseIsReturned() {
		String recordChangeType = DTFChangeType.DELETE.getDtfRecordRepresentation();
		DTFChangeType permittedChangeType = DTFChangeType.INSERT;
		
		when(mockCsvRecord.get(DTFField.RECORD_CHANGE_TYPE.getColumnNumber())).thenReturn(recordChangeType);
		when(mockCsvRecord.get(DTFField.RECORD_IDENTIFIER.getColumnNumber())).thenReturn(String.valueOf(UNPERMITTED_IDENTIFIER));
		
		assertFalse(dtfRecord.isPermittedChangeTypeAndIdentifier(permittedChangeType));
		
	}
	
	private void mockCSVRecordAndInstatiateTestInstance() {
		
		when(mockCsvRecord.get(DTFField.RECORD_CHANGE_TYPE.getColumnNumber())).thenReturn(CHANGE_TYPE.getDtfRecordRepresentation());
		when(mockCsvRecord.get(DTFField.RECORD_IDENTIFIER.getColumnNumber())).thenReturn(String.valueOf(RECORD_IDENTIFIER));
		when(mockCsvRecord.get(DTFField.RECORD_PRO_ORDER.getColumnNumber())).thenReturn(String.valueOf(RECORD_PRO_ORDER));
		dtfRecord = new DTFRecord(mockCsvRecord, VERSION);
		
	}
}
