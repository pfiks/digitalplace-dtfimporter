package com.placecube.digitalplace.address.dtf.service;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

import com.placecube.digitalplace.address.dtf.constant.SqlConstants;
import com.placecube.digitalplace.address.dtf.model.DTFChangeType;
import com.placecube.digitalplace.address.dtf.model.DTFDatabaseRecord;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
public class DatabaseServiceTest {

	private DatabaseService databaseService;
	
	@Mock
	private Properties mockConfig;
	
	@Mock
	private BasicDataSource mockBasicDataSource;
	
	@Mock
	private DTFDatabaseRecord mockDTFDatabaseRecord;
	
	@Mock
	private Connection mockConnection;
	
	@Mock
	private Statement mockStatement;
	
	@Mock
	private CallableStatement mockCallableStatement;
	
	@Before
	public void setUp() throws SQLException {
		initMocks(this);
		
		when(mockBasicDataSource.getConnection()).thenReturn(mockConnection);
		
		databaseService = ObjectFactory.createDatabaseService(mockConfig);
		databaseService.setBasicDataSource(mockBasicDataSource);
	}

	@Test
	public void processDtfDatabaseRecord_whenChangeTypeIsDelete_thenDatabaseDeleteCommandIsExecuted() throws SQLException {
		
		when(mockDTFDatabaseRecord.getDeleteSql()).thenReturn(SqlConstants.DELETE_BY_UPRN);
		when(mockConnection.createStatement()).thenReturn(mockStatement);
		
		databaseService.processDtfDatabaseRecord(mockDTFDatabaseRecord, DTFChangeType.DELETE);
		
		verify(mockDTFDatabaseRecord, times(1)).getDeleteSql();
		verifyRunSqlOccurredSent(SqlConstants.DELETE_BY_UPRN);

	}
	
	@Test
	public void processDtfDatabaseRecord_whenChangeTypeIsInsert_thenDatabaseInsertCommandIsExecuted() throws SQLException {
		
		when(mockDTFDatabaseRecord.getInsertSql()).thenReturn(SqlConstants.INSERT_BLPU);
		when(mockConnection.prepareCall(Matchers.anyString())).thenReturn(mockCallableStatement);
		
		databaseService.processDtfDatabaseRecord(mockDTFDatabaseRecord, DTFChangeType.INSERT);
		
		InOrder inOrder = inOrder(mockDTFDatabaseRecord);
		inOrder.verify(mockDTFDatabaseRecord, times(1)).getInsertSql();
		inOrder.verify(mockDTFDatabaseRecord, times(1)).getDatabaseInsertParameters();
		
		verifyCallableStatmentSent();

	}
	
	@Test
	public void processDtfDatabaseRecord_whenChangeTypeIsUpdate_thenDatabaseDeleteAndInsertCommandIsExecuted() throws SQLException {
		
		when(mockDTFDatabaseRecord.getDeleteSql()).thenReturn(SqlConstants.DELETE_BY_UPRN);
		when(mockDTFDatabaseRecord.getInsertSql()).thenReturn(SqlConstants.INSERT_BLPU);
		when(mockConnection.createStatement()).thenReturn(mockStatement);
		when(mockConnection.prepareCall(Matchers.anyString())).thenReturn(mockCallableStatement);
		
		databaseService.processDtfDatabaseRecord(mockDTFDatabaseRecord, DTFChangeType.UPDATE);

		verify(mockDTFDatabaseRecord, times(1)).getDeleteSql();
		
		verifyRunSqlOccurredSent(SqlConstants.DELETE_BY_UPRN);
		
		
		InOrder inOrder = inOrder(mockDTFDatabaseRecord);
		inOrder.verify(mockDTFDatabaseRecord, times(1)).getInsertSql();
		inOrder.verify(mockDTFDatabaseRecord, times(1)).getDatabaseInsertParameters();
		
		verifyCallableStatmentSent();

	}
	
	public void verifyRunSqlOccurredSent(String sql) throws SQLException {
		InOrder inOrder = inOrder(mockBasicDataSource, mockConnection, mockStatement);
		
		inOrder.verify(mockBasicDataSource, times(1)).getConnection();
		inOrder.verify(mockConnection, times(1)).createStatement();
		inOrder.verify(mockStatement, times(1)).execute(sql);
	}
	
	public void verifyCallableStatmentSent() throws SQLException {
		InOrder inOrder = inOrder(mockBasicDataSource, mockConnection, mockCallableStatement);
		
		inOrder.verify(mockBasicDataSource, times(1)).getConnection();
		inOrder.verify(mockConnection, times(1)).prepareCall(Matchers.anyString());
		inOrder.verify(mockCallableStatement, times(1)).execute();
		
	}

}
