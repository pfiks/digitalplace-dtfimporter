package com.placecube.digitalplace.address.dtf.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public enum BlpuField {
	
	UPRN(3, Long.class),
	LOGICAL_STATUS(4, Long.class),
	BLPU_STATE(5, Long.class),
	BLPU_STATE_DATE(6, LocalDate.class),
	BLPU_CLASS(7, String.class),
	PARENT_UPRN(8, Long.class),
	X_COORDINATE(9, BigDecimal.class),
	Y_COORDINATE(10, BigDecimal.class),
	RPC(11, Long.class),
	LOCAL_CUSTODIAN_CODE(12, Long.class),
	START_DATE(13, LocalDate.class),
	END_DATE(14, LocalDate.class),
	LAST_UPDATE_DATE(15, LocalDate.class),
	ENTRY_DATE(16, LocalDate.class),
	ORGANISATION(17, String.class),
	WARD_CODE(18, String.class),
	PARISH_CODE(19, String.class),
	CUSTODIAN_ONE(20, Long.class),
	CUSTODIAN_TWO(21, Long.class),
	CAN_KEY(22, String.class);
	
	private int columnNumber;
	
	private Class<?> fieldClass;
	
	private static List<BlpuField> fieldsInColumnOrder;
	
	static {
		fieldsInColumnOrder = Arrays.asList(BlpuField.values())
				.stream()
				.sorted(Comparator.comparingInt(BlpuField::getColumnNumber))
				.collect(Collectors.toList());
	}
	
	BlpuField(int columnNumber, Class<?> fieldClass) {
		this.columnNumber = columnNumber;
		this.fieldClass = fieldClass;
	}
	
	public int getColumnNumber() {
		return columnNumber;
	}
	
	public Class<?> getFieldClass() {
		return fieldClass;
	}
	
	public static List<BlpuField> getFieldsInColumnOrder() {
		return fieldsInColumnOrder;
	}

}
