package com.placecube.digitalplace.address.dtf.service;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import com.placecube.digitalplace.address.dtf.constant.ConfigKeys;

public class ActionExecutorService {

	private ArgumentsService argumentsService;

	private DatabaseService databaseService;

	private FileAndFolderService fileAndFolderService;

	private Properties configProperties;

	private ActionExecutorService(ArgumentsService argumentsService) throws Exception {
		setArgumentsService(argumentsService);

		configProperties = ObjectFactory.createProperties(argumentsService.getConfigFilePath());

		databaseService = ObjectFactory.createDatabaseService(configProperties);

		fileAndFolderService = ObjectFactory.createFileAndFolderService(databaseService);
	}

	public static ActionExecutorService fromArgumentsService(ArgumentsService argumentsService) throws Exception {
		return new ActionExecutorService(argumentsService);
	}

	public void executeAction() throws Exception {
		String action = argumentsService.getAction();

		if (ArgumentsService.ACTION_LOAD_FILE.equalsIgnoreCase(action)) {

			executeLoadFileAction();

		} else if (ArgumentsService.ACTION_CLEAR.equalsIgnoreCase(action)) {

			executeClearAll();
		}
	}

	private void executeClearAll() throws IOException {

		MessageService.printMessage("Clearing the whole database? (y/n)");
		if (PromptService.promtpForConfirmation()) {

			MessageService.printMessage("Clearing the whole database.");
			databaseService.clearEverything();

		} else {

			MessageService.printMessage("You cancelled the database clear command.");

		}

	}

	private void executeLoadFileAction() throws Exception {
		String fileOrFolderName = configProperties.getProperty(ConfigKeys.DTF_SOURCE_FILE_FOLDER);

		File sourceFolderOrFile = new File(fileOrFolderName);
		if (sourceFolderOrFile.isDirectory()) {
			fileAndFolderService.processFolder(sourceFolderOrFile);
		} else {
			fileAndFolderService.processFile(sourceFolderOrFile);
		}
	}
	
	public void setArgumentsService(ArgumentsService argumentsService) {
		this.argumentsService = argumentsService;
	}

	public void setConfigProperties(Properties configProperties) {
		this.configProperties = configProperties;
	}

	public void setDatabaseService(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}

	public void setFileAndFolderService(FileAndFolderService fileAndFolderService) {
		this.fileAndFolderService = fileAndFolderService;
	}

}
