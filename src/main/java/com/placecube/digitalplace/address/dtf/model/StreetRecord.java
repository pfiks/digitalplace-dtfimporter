package com.placecube.digitalplace.address.dtf.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.placecube.digitalplace.address.dtf.constant.SqlConstants;

/*
 * Street Record - DTF Record Identifier 11
 */
public class StreetRecord implements DTFDatabaseRecord {

	private DTFRecord dtfRecord;
	
	private Map<StreetRecordField, Object> fields = new HashMap<>();
	
	public StreetRecord(DTFRecord dtfRecord) {
		this.dtfRecord = dtfRecord;
		for (StreetRecordField streetRecordField : StreetRecordField.values()) {
			int columnIndex = streetRecordField.getColumnNumber();
			fields.put(streetRecordField,
					dtfRecord.columnValueToObject(columnIndex, streetRecordField.getFieldClass()));
		}

	}

	@Override
	public List<Object> getDatabaseInsertParameters() {
		List<Object> params = new ArrayList<>();

		for (StreetRecordField streetRecordField : StreetRecordField.getFieldsInPositionOrder()) {
			params.add(fields.get(streetRecordField));
		}

		params.add(dtfRecord.getVersionId());

		return params;
	}

	@Override
	public String getDeleteSql() {
		return String.format(SqlConstants.DELETE_BY_USRN, SqlConstants.TABLE_STREET_RECORD,
				dtfRecord.getColumnValue(StreetRecordField.USRN.getColumnNumber()));
	}

	@Override
	public String getInsertSql() {
		return SqlConstants.INSERT_STREET_RECORD;
	}

}
