///usr/bin/env jbang "$0" "$@" ; exit $?

//JAVA 8

//DEPS mysql:mysql-connector-java:8.0.33
//DEPS org.apache.commons:commons-csv:1.8
//DEPS org.apache.commons:commons-dbcp2:2.0.1
//DEPS org.apache.commons:commons-lang3:3.10
//DEPS org.apache.logging.log4j:log4j-api:2.13.1
//DEPS org.apache.logging.log4j:log4j-core:2.13.1

//SOURCES **/*.java

package com.placecube.digitalplace.address.dtf;

import java.time.LocalDateTime;

import com.placecube.digitalplace.address.dtf.service.ActionExecutorService;
import com.placecube.digitalplace.address.dtf.service.ArgumentsService;
import com.placecube.digitalplace.address.dtf.service.MessageService;
import com.placecube.digitalplace.address.dtf.service.ObjectFactory;

public class Program {

	private static ArgumentsService argumentsService;

	private static ActionExecutorService actionExecutorService;

	private static void init(String[] args) throws Exception {

		argumentsService = ObjectFactory.createArgumentsService(args);
		
		actionExecutorService = ObjectFactory.createActionExecutorService(argumentsService);
		
	}

	public static void main(String[] args) {

		MessageService.printMessage("Started at " + LocalDateTime.now());
		try {

			init(args);
			actionExecutorService.executeAction();

		} catch (Exception e) {
			MessageService.printMessage("Error:" + e.getMessage());
			e.printStackTrace();
		}

		MessageService.printMessage("Ended at " + LocalDateTime.now());

	}
}
