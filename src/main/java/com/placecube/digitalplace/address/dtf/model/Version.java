package com.placecube.digitalplace.address.dtf.model;

import java.time.LocalDateTime;

public class Version {
	public int versionId;
	public LocalDateTime loadDateTime;
	public String sourceFilename;

	public int getVersionId() {
		return versionId;
	}

	public void setVersionId(int versionId) {
		this.versionId = versionId;
	}

	public LocalDateTime getLoadDateTime() {
		return loadDateTime;
	}

	public void setLoadDateTime(LocalDateTime loadDateTime) {
		this.loadDateTime = loadDateTime;
	}

	public String getSourceFilename() {
		return sourceFilename;
	}

	public void setSourceFilename(String sourceFilename) {
		this.sourceFilename = sourceFilename;
	}

}
