package com.placecube.digitalplace.address.dtf.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

import com.placecube.digitalplace.address.dtf.constant.DTFConstants;
import com.placecube.digitalplace.address.dtf.constant.DateFormatConstants;
import com.placecube.digitalplace.address.dtf.constant.SqlConstants;
import com.placecube.digitalplace.address.dtf.service.DatabaseService;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
public class LpiTest {
	
	private static final DTFChangeType CHANGE_TYPE = DTFChangeType.INSERT;
	
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DateFormatConstants.DTF_RECORD_DATE_FORMAT);
	
	private static final int RECORD_IDENTIFIER = DTFConstants.INDENTIFIER_BLPU;
	
	private static final long RECORD_PRO_ORDER = 1l;
	
	private static final long UPRN = 12345;
	
	private static final int VERSION = 1;
	
	private Lpi lpi;

	@Mock
	private DatabaseService mockDatabaseService;
	
	@Mock
	private DTFRecord mockDtfRecord;
	
	@Before
	public void setUp() {
		initMocks(this);
		
		when(mockDtfRecord.getDtfChangeType()).thenReturn(CHANGE_TYPE);
		when(mockDtfRecord.getRecordIdentifier()).thenReturn(RECORD_IDENTIFIER);
		when(mockDtfRecord.getProOrder()).thenReturn(RECORD_PRO_ORDER);
		when(mockDtfRecord.getVersionId()).thenReturn(VERSION);
		
		lpi = new Lpi(mockDtfRecord);
		
	}
	
	@Test
	public void getDatabaseInsertParameters_whenCalled_thenCorrectInsertParametersReturned() {
		
		List<Object> expectedParameters = new ArrayList<>(); 
		for (LpiField lpiField : LpiField.values()) {
			String value = null;
			
			if (lpiField.getFieldClass() == LocalDate.class) {
				value = LocalDate.ofEpochDay(Long.valueOf(lpiField.getColumnNumber())).format(DATE_FORMATTER);
			} else {
				value = String.valueOf(lpiField.getColumnNumber());
			}
			
			when(mockDtfRecord.columnValueToObject(lpiField.getColumnNumber(), lpiField.getFieldClass())).thenReturn(value);
			expectedParameters.add(value);
		}
		expectedParameters.add(String.valueOf(VERSION));

		lpi = new Lpi(mockDtfRecord);
		List<Object> actualParameters = lpi.getDatabaseInsertParameters()
				.stream()
				.map(item -> String.valueOf(item))
				.collect(Collectors.toList());
		
		assertThat(actualParameters, IsIterableContainingInOrder.contains(expectedParameters.toArray()));
		
	}

	@Test
	public void getDeleteSql_whenCalled_thenCorrectSqlStatementIsReturned() {
		when(mockDtfRecord.getColumnValue(LpiField.UPRN.getColumnNumber())).thenReturn(String.valueOf(UPRN));
		
		String actualSql = lpi.getDeleteSql();
		String testSql = String.format(SqlConstants.DELETE_BY_UPRN, SqlConstants.TABLE_LPI, UPRN);
		
		assertEquals(testSql, actualSql);

	}
	
	@Test
	public void getInsertSql_whenCalled_thenCorrectSqlStatementIsReturned() {
		
		String sql = lpi.getInsertSql();
		
		assertEquals(SqlConstants.INSERT_LPI, sql);
		
	}

}
