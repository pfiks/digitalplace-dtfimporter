package com.placecube.digitalplace.address.dtf.constant;

import java.util.HashSet;
import java.util.Set;

public final class DTFConstants {

	public static final Set<Integer> ALLOWED_RECORD_INDENTIFIERS = new HashSet<>();
	
	public static final int INDENTIFIER_BLPU = 21;
	public static final int INDENTIFIER_LPI = 24;
	public static final int INDENTIFIER_STREET_DESCRIPTOR = 15;
	public static final int INDENTIFIER_STREET_RECORD = 11;
	
	static {
		
		ALLOWED_RECORD_INDENTIFIERS.add(INDENTIFIER_BLPU);
		ALLOWED_RECORD_INDENTIFIERS.add(INDENTIFIER_LPI);
		ALLOWED_RECORD_INDENTIFIERS.add(INDENTIFIER_STREET_DESCRIPTOR);
		ALLOWED_RECORD_INDENTIFIERS.add(INDENTIFIER_STREET_RECORD);
		
	}

}
