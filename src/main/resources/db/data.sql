/****** Object:  Table RecordIdentifier    Script Date: 02/16/2017 12:28:21 ******/
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (10, N'Header');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (11, N'Street Record');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (12, N'Street Cross Reference');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (13, N'Elementary Street Unit');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (14, N'ESU Co-ordinate');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (15, N'Street Descriptor');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (21, N'Basic Land and Property Unit');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (22, N'Provenance');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (23, N'Application Cross Reference');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (24, N'Land and Property Identifier');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (25, N'BLPU Extent');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (26, N'BLPU Extent Polygon');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (27, N'BLPU Extent Polygon Vertex');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (29, N'LLPG Metadata');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (98, N'Key Sequence');
INSERT RecordIdentifier (RecordIdentifierId, Description) VALUES (99, N'Trailer');

