package com.placecube.digitalplace.address.dtf.service;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

import com.placecube.digitalplace.address.dtf.model.DTFChangeType;

public class FileAndFolderService {
	
	private DatabaseService databaseService;
	
	private DTFParseService dtfParseService;
	
	public FileAndFolderService(DatabaseService databaseService) {
		this.databaseService = databaseService;
		this.dtfParseService = ObjectFactory.createFileParseService(databaseService);
	}
	
	public void processFile(File file) throws Exception {
		MessageService.printMessage("Importing DTF file:" + file.getAbsolutePath());
		
		boolean processedAlready = databaseService.isVersionExists(file.getName());
		if (!processedAlready) {
			dtfParseService.updateDatabaseFromFile(file, DTFChangeType.DELETE);
			dtfParseService.updateDatabaseFromFile(file, DTFChangeType.INSERT);
			dtfParseService.updateDatabaseFromFile(file, DTFChangeType.UPDATE);	
		} else {
			MessageService.printMessage("Excluding (Already processed) - " + file.getAbsolutePath());
		}
	}
	
	public void processFolder(File folder) throws Exception {
		MessageService.printMessage("Importing DTF folder:" + folder.getAbsolutePath());
		List<File> files = DTFFileCollector.getFilesFromFolder(Paths.get(folder.getAbsolutePath()), ".csv");
		files.sort(DTFFileCollector.getDTFFileComparator());
		
		for (File file : files) {
			processFile(file);
		}
	}
	
	public void setFileParseService(DTFParseService dtfParseService) {
		this.dtfParseService = dtfParseService;
	}
	
	public void setDatabaseService(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}

}
